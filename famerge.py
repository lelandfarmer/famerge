import openpyxl
import csv
import os
import time
import datetime

consolidate_inventory_csv = 'FA-Inventory-Consolidated-' + time.strftime('%Y-%m-%d-%H-%M-%S') + '.csv'


def import_purchase_order(file):
    start_row = 15
    barcode_col = 'B'
    sn_col = 'C'
    site_cell = openpyxl.utils.get_column_letter(1)
    mk_col = openpyxl.utils.get_column_letter(4)
    md_col = openpyxl.utils.get_column_letter(5)
    r_col = openpyxl.utils.get_column_letter(2)
    
    # Open the work book
    wb = openpyxl.load_workbook(file)
    sheet_name = wb.get_sheet_names()[0]
    sh = wb.get_sheet_by_name(sheet_name)
    
    # Get PO, Site, Make, and Model 
    po_number = str(os.path.split(file)[1:]).split('.')
    po_number = str(po_number[0])[2:]
    
    date_received = sh.cell(str(r_col) + str(10)).value
    site = sh.cell(str(site_cell) + str(start_row)).value
    make = sh.cell(str(mk_col) + str(start_row)).value
    model = sh.cell(str(md_col) + str(start_row)).value
    
    # Get Tuple of SN# and Barcode/Tag #'s
    serials = sh[sn_col]
    serials = serials[start_row -1::]
    
    barcode = sh[barcode_col]
    barcode = barcode[start_row - 1::]
    
    tags = dict(zip(barcode, serials))
    
    po = [po_number, site, make, model, tags, date_received]
    
    return po


def create_fa_inventory(fixed_assets_path):
    start_time = time.time()
    total = len(os.listdir(fixed_assets_path))
    count = total
    lines = 0
    with open(consolidate_inventory_csv, 'wt', newline='') as csvfile:
        wrpo = csv.writer(csvfile) 
        wrpo.writerow(['DATE SCANNED', 'DATE RECEIVED', 'PO#', 'SITE', 'MAKE', 'MODEL', 'VUSD TAG', 'SN#'])
        for file in os.listdir(fixed_assets_path):
            complete_path = fixed_assets_path + '\\' + file
            try:
                purchase_order = import_purchase_order(complete_path)
                tags = purchase_order[4]
                print(count, 'of', total, '-- processing', complete_path, end="\r", flush=True)
                time_stamp = os.path.getmtime(os.path.join(fixed_assets_path, file))
                date_created = datetime.datetime.fromtimestamp(time_stamp).date()
                for barcode, serial in tags.items():
                        lines += 1
                        wrpo.writerow([date_created, 
                                       purchase_order[5],
                                       purchase_order[0],
                                       purchase_order[1],
                                       purchase_order[2],
                                       purchase_order[3],
                                       barcode.value,
                                       serial.value])             
            except openpyxl.utils.exceptions.InvalidFileException:
                print(count, 'of', total,'-- Invalid file...skipping')
            count -= 1 
   
    elapsed_time = time.time() - start_time
    inventory_path = os.path.join(os.getcwd(), consolidate_inventory_csv)
    print('\nTotal File Processed:', total)
    print('Elapsed Time: ', elapsed_time/60, 'Mins')
    print('Lines Written: ', lines)
    print('All purchase order spreadsheets have been merged into : ')
    print(inventory_path)


def main():
    folder_name = 'MERGEIT-test'
    print('                                 Famerge \n')
    print('1st - Copy the FA inventory network share to the same dir that you ran Famerge.')
    print('2nd - Rename the folder to', folder_name)
    ans = str(input('Would you like to continue(Y/N)?: ')).lower()
  
    if ans == 'y':
        create_fa_inventory(os.path.join(os.getcwd(), folder_name))
    else:
        print('Program terminated by user...Goodbye!')


if __name__=="__main__": 
    main()
else:
    print('faimerge.py is being imported from another module')